package sk.jd.sdk

import android.content.Context
import sk.jd.sdk.internal.SdkImpl
import sk.jd.sdk.internal.enviroment.Env

object SdkFactory {
    private var instance: SdkImpl? = null

    fun getInstance(context: Context): Sdk {
        instance.let {
            return if (it != null)
                it
            else {
                instance = SdkImpl(context, Env.TEST_ENV)
                getInstance(context)
            }
        }
    }
}