package sk.jd.sdk.model.user

import com.squareup.moshi.Json

data class UserResponse(
    @field:Json(name = "data") val user: User? = null,
    @field:Json(name = "ad") val company: Company? = null
)