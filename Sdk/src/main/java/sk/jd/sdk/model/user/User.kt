package sk.jd.sdk.model.user

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: Int? = null,
    val email: String? = null,
    @field:Json(name = "first_name") val firstName: String? = null,
    @field:Json(name = "last_name") val lastName: String? = null,
    val avatar: String? = null
) : Parcelable {
    val fullName: String
        get() = "$firstName $lastName".trimEnd()
}