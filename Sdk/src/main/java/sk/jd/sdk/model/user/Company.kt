package sk.jd.sdk.model.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Company(
    val company: String? = null,
    val url: String? = null,
    val text: String? = null
): Parcelable