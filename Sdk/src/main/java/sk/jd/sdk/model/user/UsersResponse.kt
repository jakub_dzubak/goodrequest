package sk.jd.sdk.model.user

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UsersResponse(
    val page: Int? = null,
    @field:Json(name = "per_page") val perPage: Int? = null,
    val total: Int? = null,
    @field:Json(name = "total_pages") val totalPages: Int? = null,
    @field:Json(name = "data") val users: List<User>? = null,
    @field:Json(name = "ad") val company: Company? = null
): Parcelable