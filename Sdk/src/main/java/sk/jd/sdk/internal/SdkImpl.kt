package sk.jd.sdk.internal

import android.content.Context
import sk.jd.sdk.Sdk
import sk.jd.sdk.UserManager
import sk.jd.sdk.internal.enviroment.Environment
import sk.jd.sdk.internal.managers.UserManagerImpl

internal data class SdkContext(val context: Context, val environment: Environment)

internal class SdkImpl(context: Context, environment: Environment) : Sdk {

    private val testCommunication: TestCommunication = TestCommunication(SdkContext(context, environment))
    private val userManager: UserManager by lazy { UserManagerImpl(testCommunication.api) }

    override fun userManager(): UserManager = userManager
}
