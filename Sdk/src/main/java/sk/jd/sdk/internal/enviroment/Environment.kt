package sk.jd.sdk.internal.enviroment

internal interface Environment {
    fun getUrl(): String
}

internal enum class Env(private val url: String): Environment {
    TEST_ENV("https://reqres.in");

    override fun getUrl() = url
}