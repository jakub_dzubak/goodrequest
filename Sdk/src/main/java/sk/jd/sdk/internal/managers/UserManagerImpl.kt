package sk.jd.sdk.internal.managers

import android.os.HandlerThread
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import sk.jd.sdk.UserManager
import sk.jd.sdk.internal.TestApi
import sk.jd.sdk.model.user.User

internal class UserManagerImpl(private val api: TestApi) : UserManager {

    private val scheduler: Scheduler

    init {
        val handlerThread = HandlerThread("UserManagerThread")
        handlerThread.start()
        scheduler = AndroidSchedulers.from(handlerThread.looper)
    }

    override fun getUsers(page: Int, size: Int): Single<List<User>> {
        return api.getUsers(page, size)
            .map { it.users.orEmpty() }
            .subscribeOn(scheduler)
    }

    override fun getUser(id: Int): Single<User> {
        return api.getUser(id)
            .map { it.user ?: User() }
            .subscribeOn(scheduler)
    }

}