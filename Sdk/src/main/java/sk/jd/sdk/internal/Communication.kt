package sk.jd.sdk.internal

import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

internal abstract class AbsCommunication<API_INTERFACE>(
    baseUrl: String,
    apiClass: Class<API_INTERFACE>
) {

    private val client: OkHttpClient

    val api: API_INTERFACE

    init {
        val clientBuilder = OkHttpClient.Builder()
//        if (BuildConfig.DEBUG) {
        if (true) {
            val logging =
                HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("OkHttp", it) })
            logging.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(logging)
        }
        clientBuilder.connectTimeout(1, TimeUnit.MINUTES)
        clientBuilder.readTimeout(1, TimeUnit.MINUTES)
        clientBuilder.writeTimeout(1, TimeUnit.MINUTES)
        client = clientBuilder.build()

        val retrofitBuilder = Retrofit.Builder()
        retrofitBuilder.baseUrl(baseUrl)
        retrofitBuilder.client(client)


        val moshi = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .build()
        val factory: Converter.Factory = MoshiConverterFactory.create(moshi)

        retrofitBuilder.addConverterFactory(factory)
        retrofitBuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        retrofitBuilder.validateEagerly(true)
        val retrofit = retrofitBuilder.build()
        api = retrofit.create<API_INTERFACE>(apiClass)
    }

}

internal class TestCommunication(sdkContext: SdkContext): AbsCommunication<TestApi>(
    sdkContext.environment.getUrl(),
    TestApi::class.java
)