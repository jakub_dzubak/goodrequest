package sk.jd.sdk.internal

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import sk.jd.sdk.model.user.UserResponse
import sk.jd.sdk.model.user.UsersResponse

const val API = "/api/"
const val USERS = "users"

internal interface TestApi {

    @GET("$API$USERS")
    fun getUsers(@Query("page") page: Int, @Query("per_page") size: Int): Single<UsersResponse>

    @GET("$API$USERS/{id}")
    fun getUser(@Path("id") id: Int): Single<UserResponse>

}