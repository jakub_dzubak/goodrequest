package sk.jd.sdk

import io.reactivex.Single
import sk.jd.sdk.model.user.User

interface UserManager {

    fun getUsers(page: Int = 1, size: Int = 5): Single<List<User>>

    fun getUser(id: Int): Single<User>

}