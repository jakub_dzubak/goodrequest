package sk.jd.goodrequest.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import kotlinx.android.synthetic.main.activity_main.*
import sk.jd.goodrequest.R
import sk.jd.goodrequest.ui.userdetail.USER_ID

class MainActivity : AppCompatActivity() {

    private val isTablet
        get() = resources.getBoolean(R.bool.isTablet)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.title = getString(R.string.title_users)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    fun showDetail(userId: String) {
        when {
            isTablet -> {
                findNavController(this, R.id.detailContainer).navigate(
                    R.id.userDetailFragment,
                    Bundle().apply { putString(USER_ID, userId) })
            }
            else -> {
                val intent = Intent(this, DetailActivity::class.java).apply { putExtra(USER_ID, userId) }
                startActivity(intent)
            }
        }
    }

}
