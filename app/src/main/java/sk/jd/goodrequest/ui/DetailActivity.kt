package sk.jd.goodrequest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.activity_main.*
import sk.jd.goodrequest.R
import sk.jd.goodrequest.ui.userdetail.USER_ID

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        toolbar.title = getString(R.string.titlet_user_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            findNavController(R.id.detailContainer).navigate(
                R.id.userDetailFragment,
                Bundle().apply { putString(USER_ID, intent.getStringExtra(USER_ID)) }
            )
        }
    }

}