package sk.jd.goodrequest.ui.userdetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_user_detail.*
import org.koin.android.ext.android.get
import sk.jd.goodrequest.R
import sk.jd.goodrequest.ui.base.AbsBaseFragment
import sk.jd.goodrequest.ui.extensions.visibleIf
import sk.jd.sdk.model.user.User

const val USER_ID = "USER_ID"

class UserDetailFragment : AbsBaseFragment() {

    private val vm: UserViewModel by viewModels { get<UserViewModelFactory>() }
    override val resourceId: Int = R.layout.fragment_user_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresh()
        vm.userDetailData.observe(viewLifecycleOwner, Observer(this::render))
        swipeToRefresh.setOnRefreshListener { refresh() }
    }

    private fun refresh() {
        arguments?.getString(USER_ID)?.let {
            try {
                vm.loadUser(it.toInt())
            } catch (ex: NumberFormatException) {
                render(UserDetailState.NotSelected)
            }
        } ?: render(UserDetailState.NotSelected)
    }

    private fun render(state: UserDetailState) {
        when (state) {
            is UserDetailState.Data -> showContent(state.user)
            is UserDetailState.Error -> showError(state.throwable)
            is UserDetailState.NotSelected -> {
            }
            UserDetailState.Loading -> {
            }
        }
        contentGroup.visibleIf(state is UserDetailState.Data)
        error.visibleIf(state is UserDetailState.Error)
        swipeToRefresh.isRefreshing = state is UserDetailState.Loading
    }

    private fun showContent(user: User) {
        user.avatar?.let {
            Glide.with(requireContext())
                .load(it)
                .placeholder(R.drawable.ic_account_circle_24px)
                .error(R.drawable.ic_account_circle_24px)
                .apply(RequestOptions.circleCropTransform())
                .into(avatar)
        }
        name.text = user.fullName
        email.text = user.email
    }

    private fun showError(throwable: Throwable) {
        startPostponedEnterTransition()
        Snackbar.make(
            requireView(),
            throwable.localizedMessage ?: "Fail to load user detail information",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction(getString(R.string.retry)) { refresh() }
            .show()
    }

}