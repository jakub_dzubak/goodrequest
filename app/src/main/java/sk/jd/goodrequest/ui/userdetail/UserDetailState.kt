package sk.jd.goodrequest.ui.userdetail

import sk.jd.sdk.model.user.User

sealed class UserDetailState {
    data class Data(val user: User): UserDetailState()
    data class Error(val throwable: Throwable): UserDetailState()
    object Loading: UserDetailState()
    object NotSelected: UserDetailState()
}