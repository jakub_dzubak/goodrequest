package sk.jd.goodrequest.ui.userslist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_users_list.*
import org.koin.android.ext.android.get
import sk.jd.goodrequest.R
import sk.jd.goodrequest.ui.MainActivity
import sk.jd.goodrequest.ui.base.AbsBaseFragment


class UsersListFragment : AbsBaseFragment() {

    private val vm: UsersViewModel by viewModels { get<UsersViewModelFactory>() }
    override val resourceId: Int = R.layout.fragment_users_list
    private lateinit var adapter: UsersAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initSwipeToRefresh()
    }

    private fun initAdapter() {
        val isTablet = resources.getBoolean(R.bool.isTablet)
        recyclerView.addItemDecoration(VerticalSpaceItemDecoration(20))
        adapter = UsersAdapter(isTablet) { userId -> (requireActivity() as? MainActivity)?.showDetail(userId.toString()) }
        recyclerView.adapter = adapter.withLoadStateFooter(UsersLoadStateAdapter())
        adapter.addLoadStateListener {
            val state = it.refresh
            swipeToRefresh.isRefreshing = state == LoadState.Loading
            if (state is LoadState.Error) {
                Snackbar.make(
                    requireView(),
                    state.error.localizedMessage ?: "Unknown error",
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(getString(R.string.retry)) { adapter.retry() }
                    .show()
            }
        }
        vm.users.observe(viewLifecycleOwner, Observer { data ->
            adapter.submitData(lifecycle, data)
        })
    }

    private fun initSwipeToRefresh() {
        swipeToRefresh.setOnRefreshListener { adapter.refresh() }
    }

}