package sk.jd.goodrequest.ui.userslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_load.view.*
import sk.jd.goodrequest.R

class UsersLoadStateViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_load, parent, false)
) {

    fun bind(loadState: LoadState) {
        with(itemView) {
            progressBar.isVisible = loadState is LoadState.Loading
        }
    }
}

class UsersLoadStateAdapter: LoadStateAdapter<UsersLoadStateViewHolder>(){
    override fun onBindViewHolder(holder: UsersLoadStateViewHolder, loadState: LoadState){
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): UsersLoadStateViewHolder = UsersLoadStateViewHolder(parent)

}