package sk.jd.goodrequest.ui.extensions

import android.view.View
import androidx.annotation.MainThread

@MainThread
fun View?.visibleIf(isVisible: Boolean) {
    this?.let {
        it.visibility = if (isVisible) View.VISIBLE else View.GONE
    }
}
