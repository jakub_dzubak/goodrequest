package sk.jd.goodrequest.ui.userslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import sk.jd.goodrequest.data.UsersRepository
import sk.jd.sdk.model.user.User


class UsersViewModel(usersRepository: UsersRepository) : ViewModel() {

    val users: LiveData<PagingData<User>> = usersRepository.getUsers(5).cachedIn(viewModelScope)

}

class UsersViewModelFactory(
    private val usersRepository: UsersRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UsersViewModel(usersRepository) as T
    }
}