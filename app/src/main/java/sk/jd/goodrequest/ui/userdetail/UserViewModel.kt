package sk.jd.goodrequest.ui.userdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import sk.jd.sdk.UserManager

class UserViewModel(private val userManager: UserManager) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val _userDetailData: MutableLiveData<UserDetailState> = MutableLiveData()
    val userDetailData: LiveData<UserDetailState>
        get() = _userDetailData

    fun loadUser(id: Int) {
        userManager
            .getUser(id)
            .doOnSubscribe {
                _userDetailData.postValue(UserDetailState.Loading)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _userDetailData.postValue(UserDetailState.Data(it))
            }, {
                _userDetailData.postValue(UserDetailState.Error(it))
            })
            .toDisposables()
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    private fun Disposable.toDisposables() {
        compositeDisposable.add(this)
    }

}

class UserViewModelFactory(
    private val userManager: UserManager
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(userManager) as T
    }
}