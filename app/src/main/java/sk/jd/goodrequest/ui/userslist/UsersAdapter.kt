package sk.jd.goodrequest.ui.userslist

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_user.view.*
import sk.jd.goodrequest.R
import sk.jd.sdk.model.user.User

class UsersAdapter(
    private val showSelectedPosition: Boolean,
    private val onUserSelected: (Int) -> Unit
) : PagingDataAdapter<User, RecyclerView.ViewHolder>(UserComparator()) {

    private var selectedElementPosition = Adapter.NO_SELECTION

    private val clickListener: (Int, Int) -> Unit = { id, position ->
        onUserSelected.invoke(id)
        handleSelection(position)
    }

    private fun handleSelection(position: Int) {
        if (showSelectedPosition) {
            val oldPosition = selectedElementPosition
            selectedElementPosition = position
            if (oldPosition != Adapter.NO_SELECTION)
                notifyItemChanged(oldPosition)
            notifyItemChanged(selectedElementPosition)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val user = getItem(position)
        if (holder is UserViewHolder) {
            user?.let { holder.bind(it) }
            holder.itemView.isSelected = showSelectedPosition && selectedElementPosition == position
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        UserViewHolder(parent, clickListener)

}

private class UserViewHolder(
    val parent: ViewGroup,
    val onUserSelected: (Int, Int) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
) {

    fun bind(user: User) {
        with(itemView) {
            itemView.setOnClickListener {
                user.id?.let {
                    onUserSelected.invoke(
                        it,
                        absoluteAdapterPosition
                    )
                }
            }
            name.text = user.fullName
            user.avatar?.let {
                Glide.with(this@UserViewHolder.parent.context)
                    .load(it)
                    .placeholder(R.drawable.ic_account_circle_24px)
                    .error(R.drawable.ic_account_circle_24px)
                    .apply(RequestOptions.circleCropTransform())
                    .into(avatar)
            }
        }
    }
}

private class UserComparator : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }
}

class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = verticalSpaceHeight
    }
}