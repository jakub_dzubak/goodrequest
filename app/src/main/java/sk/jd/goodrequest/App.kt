package sk.jd.goodrequest

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import sk.jd.goodrequest.data.UsersRepositoryImpl
import sk.jd.goodrequest.data.UsersRepository
import sk.jd.goodrequest.ui.userdetail.UserViewModelFactory
import sk.jd.goodrequest.ui.userslist.UsersViewModelFactory
import sk.jd.sdk.Sdk
import sk.jd.sdk.SdkFactory

class App : Application() {

    private val sdk = module {
        single(null, createdAtStart = true, override = false) { SdkFactory.getInstance(get()) }
    }

    private val viewModelFactories = module {
        factory { UsersViewModelFactory(get()) }
        factory { UserViewModelFactory(get<Sdk>().userManager()) }
    }

    private val repositories = module {
        single { provideUserRepository(get()) }
    }

    private fun provideUserRepository(sdk: Sdk): UsersRepository = UsersRepositoryImpl(sdk)

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(listOf(sdk, viewModelFactories, repositories))
        }
    }

}