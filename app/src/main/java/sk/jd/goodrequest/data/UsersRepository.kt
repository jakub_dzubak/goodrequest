package sk.jd.goodrequest.data

import androidx.lifecycle.*
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import sk.jd.goodrequest.ui.userdetail.UserDetailState
import sk.jd.sdk.Sdk
import sk.jd.sdk.model.user.User
import java.util.*

interface UsersRepository {
    fun getUsers(pageSize: Int): LiveData<PagingData<User>>
}

class UsersRepositoryImpl(private val sdk: Sdk) : UsersRepository {

    override fun getUsers(pageSize: Int): LiveData<PagingData<User>> {
        return Pager(
            PagingConfig(pageSize = pageSize, initialLoadSize = pageSize),
            pagingSourceFactory = { UsersPagingSource(sdk) }
        ).liveData
    }

}



