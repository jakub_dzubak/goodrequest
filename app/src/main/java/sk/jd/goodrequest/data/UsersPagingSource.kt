package sk.jd.goodrequest.data

import androidx.paging.rxjava2.RxPagingSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import sk.jd.sdk.Sdk
import sk.jd.sdk.model.user.User

class UsersPagingSource(private val sdk: Sdk) : RxPagingSource<Int, User>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, User>> {
        val nextPage = params.key ?: 1
        val loadSize = params.loadSize
        return sdk.userManager()
            .getUsers(nextPage, loadSize)
            .subscribeOn(Schedulers.io())
            .map { toLoadResult(it, nextPage, loadSize) }
            .onErrorReturn { LoadResult.Error(it) }
    }

    private fun toLoadResult(users: List<User>, page: Int, size: Int): LoadResult<Int, User> {
        return LoadResult.Page(
            users,
            null,
            users.takeIf { it.isNotEmpty() && it.size >= size }?.let { page.inc() },
            LoadResult.Page.COUNT_UNDEFINED,
            LoadResult.Page.COUNT_UNDEFINED
        )
    }

}
